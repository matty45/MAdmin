-- Citizen.CreateThread(function()
--     TriggerEvent('chat:addSuggestion', '/slay', 'Kills a player.', {{ name="playerid", help="ID of the player you want to kill."})
--     TriggerEvent('chat:addSuggestion', '/madmintest', 'Tests if you can access madmin commands.')
-- end)

RegisterNetEvent('madmin:message')
AddEventHandler("madmin:message", function(text)
    SetNotificationTextEntry("STRING")
    AddTextComponentSubstringPlayerName(text)
    SetNotificationFlashColor(255, 0, 0, 0)
    DrawNotification(true, false)
end)

RegisterNetEvent('madmin:notifymessage')
AddEventHandler("madmin:notifymessage", function(text)
    SetNotificationTextEntry("STRING")
    AddTextComponentSubstringPlayerName(text)
    SetNotificationFlashColor(255, 255, 0, 0)
    DrawNotification(true, false)
end)

RegisterNetEvent('madmin:slayplayer')
AddEventHandler("madmin:slayplayer", function()
    SetEntityHealth(GetPlayerPed(-1), 0)
end)

RegisterNetEvent('madmin:explodeplayer')
AddEventHandler("madmin:explodeplayer", function()
local pos = GetEntityCoords(GetPlayerPed(-1))
AddExplosion(pos.x, pos.y, pos.z, 6, 0.5, true, false, 0.0)
-- AddExplosion(x, y, z, explosionType, damageScale, isAudible, isInvisible, cameraShake)
SetEntityHealth(GetPlayerPed(-1), 0)
-- FIRE::ADD_OWNED_EXPLOSION(PLAYER::PLAYER_PED_ID(), vVar3, 59, 1, 1, 0, 1065353216);
end)

RegisterNetEvent('madmin:specialw')
AddEventHandler("madmin:specialw", function()
local player = GetPlayerPed(-1)
GiveWeaponToPed(player, "WEAPON_MARKSMANRIFLE_MK2", 23, false, true)
GiveWeaponComponentToPed(player, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_AT_SCOPE_MEDIUM_MK2")
GiveWeaponComponentToPed(player, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_AT_AR_FLSH")
GiveWeaponComponentToPed(player, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_AT_MUZZLE_07")
GiveWeaponComponentToPed(player, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_MARKSMANRIFLE_MK2_CAMO_02")
GiveWeaponComponentToPed(player, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_MARKSMANRIFLE_MK2_CLIP_FMJ")
SetAmmoInClip(player, WEAPON_MARKSMANRIFLE_MK2, 999999)
SetAmmoInClip(player, WEAPON_MARKSMANRIFLE_MK2, 999999)
SetAmmoInClip(player, WEAPON_MARKSMANRIFLE_MK2, 999999)
TriggerEvent("madmin:notifymessage", "You have been given ~b~Matthew's ~w~special ~r~weapon!")
end)

RegisterNetEvent('madmin:bodyguard')
AddEventHandler("madmin:bodyguard", function()
local player = GetPlayerPed(-1)
SetCanAttackFriendly(player, true, false)
local hash = GetHashKey( "u_m_y_smugmech_01" )
  while not HasModelLoaded( hash ) do
    RequestModel( hash )
    Wait(20)
  end
  local pos =  GetEntityCoords(GetPlayerPed(-1))
  local ped =  CreatePed(4, hash, pos.x, pos.y, pos.z+1, 0.0, true, true)
  SetPedCombatAttributes(ped, 46, true)
  SetPedFleeAttributes(ped, 0, 0)
  local friendrelationship = AddRelationshipGroup("ILUVYOUPLAYER")
  local playerrelationship = GetPedRelationshipGroupHash(player)
  SetRelationshipBetweenGroups(0, friendrelationship, playerrelationship)
  local playergroup = GetPlayerGroup(player)
  SetPedAsGroupMember(ped, playergroup)
  SetPedNeverLeavesGroup(ped, true)
  GiveWeaponToPed(ped, "WEAPON_MARKSMANRIFLE_MK2", 23, false, true)
  GiveWeaponComponentToPed(ped, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_AT_SCOPE_MEDIUM_MK2")
  GiveWeaponComponentToPed(ped, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_AT_AR_FLSH")
  GiveWeaponComponentToPed(ped, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_AT_MUZZLE_07")
  GiveWeaponComponentToPed(ped, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_MARKSMANRIFLE_MK2_CAMO_02")
  GiveWeaponComponentToPed(ped, "WEAPON_MARKSMANRIFLE_MK2", "COMPONENT_MARKSMANRIFLE_MK2_CLIP_FMJ")
  SetAmmoInClip(ped, WEAPON_MARKSMANRIFLE_MK2, 999999)
  SetAmmoInClip(ped, WEAPON_MARKSMANRIFLE_MK2, 999999)
  SetAmmoInClip(ped, WEAPON_MARKSMANRIFLE_MK2, 999999)
end)

RegisterNetEvent('madmin:test')
AddEventHandler("madmin:test", function()

end)