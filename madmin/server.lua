-- RegisterCommand("test", function(source, args, rawCommand)
--     local target = args[1]
--     if target == nil or GetPlayerName(target) == nil then
--         TriggerClientEvent("chatMessage", -1, "MAdmin", {255, 0, 0}, "Player id not found.")
--         return
--     end
--     TriggerClientEvent("chatMessage", -1, "MAdmin", {255, 0, 0}, "Found player.")
-- end, true)

----- test if player can access madmin commands
RegisterCommand("madmintest", function(source, args, rawCommand)
    TriggerClientEvent("chatMessage", -1, "You have access to madmin :D")
end, true)
------

----- Kill a player
RegisterCommand("slay", function(source, args, rawCommand)
    local target = args[1]
    if target == nil or GetPlayerName(target) == nil then
        TriggerClientEvent("madmin:message", -1, "MAdmin: ~y~Player ID not found!")
        return
    end
    local name = GetPlayerName(target)
    TriggerClientEvent("madmin:message", -1, "MAdmin: ~r~Slayed ~b~" .. name .. "~w~.")
    TriggerClientEvent("madmin:slayplayer", target)
end, true)
------

----- Explode a player
RegisterCommand("explode", function(source, args, rawCommand)
    local target = args[1]
    if target == nil or GetPlayerName(target) == nil then
        TriggerClientEvent("madmin:message", -1, "MAdmin: ~y~Player ID not found!")
        return
    end
    local name = GetPlayerName(target)
    TriggerClientEvent("madmin:message", -1, "MAdmin: ~r~Exploded ~b~" .. name .. "~w~.")
    TriggerClientEvent("madmin:explodeplayer", target)
end, true)
------

----- Give player matthews special weapon (Use the command twice to give ammo properly)
RegisterCommand("givespecialweapon", function(source, args, rawCommand)
    local target = args[1]
    if target == nil or GetPlayerName(target) == nil then
        TriggerClientEvent("madmin:message", -1, "MAdmin: ~y~Player ID not found!")
        return
    end
    local name = GetPlayerName(target)
    TriggerClientEvent("madmin:message", -1, "MAdmin: Given Matthew's special weapon to ~b~" .. name .. "~w~.")
    TriggerClientEvent("madmin:specialw", target)
end, true)
------

----- Send special notification to player (USE QUOTES)
RegisterCommand("notify", function(source, args, rawCommand)
    local target = args[1]
    local message = args[2]
    if target == nil or GetPlayerName(target) == nil then
        TriggerClientEvent("madmin:message", -1, "MAdmin: ~y~Player ID not found!")
        return
    end
    local name = GetPlayerName(target)
    TriggerClientEvent("madmin:message", -1, "MAdmin: Sent notification to~b~ " .. name .. "~w~.")
    TriggerClientEvent("madmin:notifymessage", target, message)
end, true)
------

----- Gives a player a bodyguard
RegisterCommand("bodyguard", function(source, args, rawCommand)
    local target = args[1]
    if target == nil or GetPlayerName(target) == nil then
        TriggerClientEvent("madmin:message", -1, "MAdmin: ~y~Player ID not found!")
        return
    end
    local name = GetPlayerName(target)
    TriggerClientEvent("madmin:message", -1, "MAdmin: Given a bodyguard to ~b~" .. name .. "~w~.")
    TriggerClientEvent("madmin:bodyguard", target)
end, true)
------

----- Testing script
RegisterCommand("test", function(source, args, rawCommand)
    local target = args[1]
    if target == nil or GetPlayerName(target) == nil then
        TriggerClientEvent("madmin:message", -1, "MAdmin: ~y~Player ID not found!")
        return
    end
    local name = GetPlayerName(target)
    TriggerClientEvent("madmin:message", -1, "MAdmin: Triggered test script on~b~ " .. name .. "~w~.")
    TriggerClientEvent("madmin:test", target)
end, true)
------